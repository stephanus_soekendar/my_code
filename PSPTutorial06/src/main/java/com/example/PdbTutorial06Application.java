package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdbTutorial06Application
{

    public static void main (String[] args)
    {
        SpringApplication.run (PdbTutorial06Application.class, args);
    }
}
